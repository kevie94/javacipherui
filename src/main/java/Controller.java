import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ResourceBundle;

/**
 * Created by Caoimhín on 19/03/2017.
 */
public class Controller implements Initializable {

    private CaesarCipher caesarCipher;
    private MultiplicativeCipher multiplicativeCipher;
    private TranspositionCipher transpositionCipher;
    private AffineCipher affineCipher;
    private VigenereCipher vigenereCipher;
    private RSAKeyGeneration rsaKeyGeneration;
    private RSACipher rsaCipher;
    @FXML private Button CaesarEncrypt;
    @FXML private Button CaesarDecrypt;
    @FXML private TextArea CaesarMessage;
    @FXML private TextField CaesarKey;
    @FXML private Button MultiplicativeEncrypt;
    @FXML private Button MultiplicativeDecrypt;
    @FXML private TextArea MultiplicativeMessage;
    @FXML private TextField MultiplicativeKey;
    @FXML private Button TranspositionEncrypt;
    @FXML private Button TranspositionDecrypt;
    @FXML private TextArea TranspositionMessage;
    @FXML private TextField TranspositionKey;
    @FXML private Button AffineEncrypt;
    @FXML private Button AffineDecrypt;
    @FXML private Button AffineRandom;
    @FXML private TextArea AffineMessage;
    @FXML private TextField AffineKey;
    @FXML private Label AffineWarning;
    @FXML private Button VigenereEncrypt;
    @FXML private Button VigenereDecrypt;
    @FXML private TextArea VigenereMessage;
    @FXML private TextField VigenereKey;
    @FXML private TextField StubKeyName;
    @FXML private Button GenerateKey;
    @FXML private TextField PubKeyName;
    @FXML private TextField PrivKeyName;
    @FXML private TextField KeySize;
    @FXML private TextField FileForEncryption;
    @FXML private Button PublicKeySelector;
    @FXML private Button PrivateKeySelector;
    @FXML private Button SelectImportFile;
    @FXML private Button RSAEncrypt;
    @FXML private Button RSADecrypt;
    @FXML private TextArea RSAResultBox;
    @FXML private Label MultiplicativeWarning;


    private FileChooser fileChooser;
    private File pubKey, privKey, rsaInputFile;
    private String encryptedFile;


    @Override // This method is called by the FXMLLoader when initialization is complete
    public void initialize(URL fxmlFileLocation, ResourceBundle resources) {
        //check to see if the fxml file has been read.
        assert CaesarEncrypt != null : "fx:id=\"CaesarEncrypt\" was not injected: check your FXML file 'simple.fxml'.";

        caesarCipher = new CaesarCipher();
        multiplicativeCipher = new MultiplicativeCipher();
        MultiplicativeWarning.setWrapText(true);
        transpositionCipher = new TranspositionCipher();
        affineCipher = new AffineCipher();
        vigenereCipher = new VigenereCipher();
        rsaKeyGeneration = new RSAKeyGeneration();
        rsaCipher = new RSACipher();
        fileChooser = new FileChooser();
        fileChooser.setInitialDirectory(new File(System.getProperty("user.dir")));
        encryptedFile = "";

        CaesarEncrypt.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                String message = CaesarMessage.getText();
                int key = Integer.parseInt(CaesarKey.getText());
                String result = caesarCipher.CaesarCipher(message, true, key);
                CaesarMessage.clear();
                CaesarMessage.appendText(result);
            }
        });
        CaesarDecrypt.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                String message = CaesarMessage.getText();
                int key = Integer.parseInt(CaesarKey.getText());
                String result = caesarCipher.CaesarCipher(message, false, key);
                CaesarMessage.clear();
                CaesarMessage.appendText(result);
            }
        });

        // The error catching here might be a bit overzealous but the biginteger.modinverse function is being bizarre in certain cases.
        MultiplicativeEncrypt.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                String message = MultiplicativeMessage.getText();
                String result = "";
                int key = Integer.parseInt(MultiplicativeKey.getText());
                if (multiplicativeCipher.hasModularInverse(key, message.length()) == true) {
                    try {
                        result = multiplicativeCipher.encode(message, key);
                    } catch (ArithmeticException e) {
                        MultiplicativeWarning.setText("The key(" + key + ") and message do not have a modular inverse. Use a different key.");
                    }
                    if (result != null && result.length() > 0) {
                        MultiplicativeMessage.clear();
                        MultiplicativeMessage.appendText(result);
                    } else {
                        MultiplicativeWarning.setText("The key(" + key + ") and message do not have a modular inverse. Use a different key.");
                    }
                }
            }
        });
        MultiplicativeDecrypt.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                String message = MultiplicativeMessage.getText();
                String result = "";
                int key = Integer.parseInt(MultiplicativeKey.getText());
                if(multiplicativeCipher.hasModularInverse(key, message.length()) == true) {
                    try {
                        result = multiplicativeCipher.decode(message, key);
                    }catch(ArithmeticException e){
                        MultiplicativeWarning.setText("The key(" + key + ") and message do not have a modular inverse. Use a different key.");
                    }
                    if(result != null && result.length() > 0) {
                        MultiplicativeMessage.clear();
                        MultiplicativeMessage.appendText(result);
                    }
                }else{
                    MultiplicativeWarning.setText("The key(" + key + ") and message do not have a modular inverse. Use a different key.");
                }
            }
        });
        TranspositionEncrypt.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                String message = TranspositionMessage.getText();
                int key = Integer.parseInt(TranspositionKey.getText());
                String result = transpositionCipher.encrypt(message, key);
                TranspositionMessage.clear();
                TranspositionMessage.appendText(result);
            }
        });
        TranspositionDecrypt.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                String message = TranspositionMessage.getText();
                int key = Integer.parseInt(TranspositionKey.getText());
                String result = transpositionCipher.decrypt(message, key);
                TranspositionMessage.clear();
                TranspositionMessage.appendText(result);
            }
        });
        AffineEncrypt.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                String message = AffineMessage.getText();
                int key = Integer.parseInt(AffineKey.getText());
                boolean keyValid = affineCipher.checkKey(key);
                if(keyValid && message != null) {
                    String result = affineCipher.encode(message, key);
                    if(result != null) {
                        AffineMessage.clear();
                        AffineMessage.appendText(result);
                    }
                }else{
                    AffineWarning.setText(key + " is not a valid key.");
                }
            }
        });
        AffineDecrypt.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                String message = AffineMessage.getText();
                int key = Integer.parseInt(AffineKey.getText());
                boolean keyValid = affineCipher.checkKey(key);
                if(keyValid && message != null) {
                    String result = affineCipher.decode(message, key);
                    if(result != null) {
                        AffineMessage.clear();
                        AffineMessage.appendText(result);
                    }
                }else{
                    AffineWarning.setText(key + " is not a valid key.");
                }
            }
        });
        AffineRandom.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                int randKey = affineCipher.generateRandomKey();
                AffineKey.clear();
                AffineKey.appendText(Integer.toString(randKey));
            }
        });
        VigenereEncrypt.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                String message = VigenereMessage.getText();
                String key = VigenereKey.getText();
                String result = vigenereCipher.encrypt(message, key);
                VigenereMessage.clear();
                VigenereMessage.appendText(result);
            }
        });
        VigenereDecrypt.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                String message = VigenereMessage.getText();
                String key = VigenereKey.getText();
                String result = vigenereCipher.decrypt(message, key);
                VigenereMessage.clear();
                VigenereMessage.appendText(result);
            }
        });
        GenerateKey.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                String keyName = StubKeyName.getText();
                int keySize;
                if(KeySize.getText().length() > 0) {
                    keySize = Integer.parseInt(KeySize.getText());
                }else{
                    keySize = 1024;
                }

                try {
                    rsaKeyGeneration.makeKeyFiles(keyName, keySize);
                    System.out.flush();
                }catch(IOException e){
                    e.printStackTrace();
                }
            }
        });
        PublicKeySelector.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                fileChooser.getExtensionFilters().clear();
                fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("RSA", "*.rsa"));
                fileChooser.setTitle("Open Public Key In .RSA File Format.");
                pubKey = fileChooser.showOpenDialog(new Stage());
                if (pubKey != null) {
                    PubKeyName.clear();
                    PubKeyName.appendText(pubKey.getName());
                }
            }
        });
        PrivateKeySelector.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                fileChooser.getExtensionFilters().clear();
                fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("RSA", "*.rsa"));
                fileChooser.setTitle("Open Private Key In .RSA File Format.");
                privKey = fileChooser.showOpenDialog(new Stage());
                if (pubKey != null) {
                    PrivKeyName.clear();
                    PrivKeyName.appendText(privKey.getName());
                }

            }
        });
        SelectImportFile.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                fileChooser.getExtensionFilters().clear();
                fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("TXT", "*.txt"));
                fileChooser.setTitle("Open A File To Encrypt/Decrypt It's Content.");
                String tempFileName = "";
                if(rsaInputFile != null) {
                    tempFileName = rsaInputFile.getName();
                }
                rsaInputFile = fileChooser.showOpenDialog(new Stage());
                if (rsaInputFile != null) {
                    FileForEncryption.clear();
                    FileForEncryption.appendText(rsaInputFile.getName());
                    System.out.println(rsaInputFile.toString());
                    RSAResultBox.clear();
                    RSAResultBox.appendText(readFromFile(rsaInputFile.getAbsolutePath()));

                }else{
                    rsaInputFile = new File(tempFileName);
                }
            }
        });
        RSAEncrypt.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                encryptedFile = "encrypted.txt";
                String result = rsaCipher.encryptAndWriteToFile(rsaInputFile.toString(), "encrypted.txt", pubKey.toString());
                RSAResultBox.clear();
                RSAResultBox.appendText(result);
                FileForEncryption.clear();
                FileForEncryption.appendText(encryptedFile);
                rsaInputFile = new File("encrypted.txt");
            }
        });
        RSADecrypt.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                encryptedFile = "decrypted.txt";
                String result = rsaCipher.readFromFileAndDecrypt(rsaInputFile.toString(), "decrypted.txt", privKey.toString());
                RSAResultBox.clear();
                RSAResultBox.appendText(result);
                FileForEncryption.clear();
                FileForEncryption.appendText(encryptedFile);
                rsaInputFile = new File("decrypted.txt");
            }
        });
    }

    public String readFromFile(String fileName){
        String message;
        try {
            message = new String(Files.readAllBytes(Paths.get(fileName)));
            return message;
        }catch(IOException e){
            e.printStackTrace();
            return null;
        }
    }

    public void writeToFile(String message, String filename){
        try{
            FileWriter messageWriter = new FileWriter(filename);
            messageWriter.write(message);
            messageWriter.close();
        }catch(IOException e){
            e.printStackTrace();
        }
    }
}
