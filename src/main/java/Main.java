import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Caoimhín on 19/03/2017.
 */
public class Main extends Application{
    @Override
    public void start(Stage primaryStage) throws Exception {
        try {
            VBox page = (VBox) FXMLLoader.load(Main.class.getResource("/Layout.fxml"));
            Scene scene = new Scene(page);
            primaryStage.getIcons().add(new Image("http://icons.iconarchive.com/icons/icons8/windows-8/32/Security-Key-Security-icon.png"));
            primaryStage.setScene(scene);
            primaryStage.setTitle(" Java Cipher          Kevin O'Sullivan - 12520327");
            primaryStage.setWidth(750);
            primaryStage.setResizable(false);
            primaryStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static void main(String[] args) {
        Application.launch(Main.class, (java.lang.String[])null);
    }

}
